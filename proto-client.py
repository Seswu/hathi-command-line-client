#!/usr/bin/python3

import socket
import json
import logging
import unittest
from urllib.parse import urlparse


def prettify(obj):
    """
    Format and indent object data.

    For use with eg. pretty-printing in order to present standardized output.
    """
    return json.dumps(obj, sort_keys=True, indent=4)


def json_prettify(json_text):
    """
    Format and indent json text data.

    For use with eg. pretty-printing in order to present standardized output.
    """
    json_data = json.loads(json_text)
    return prettify(json_data)


def load_json_file(filepath):
    """Load initialization data from file"""
    try:
        data_file = open(filepath, 'r')
        data = json.loads(data_file.read())
    finally:
        data_file.close()
    return data


def is_url(url):
    """Tests if a text string is a valid url or not"""
    try:
        urlparse(url)
        return True
    except ValueError:
        return False


class RequestID:
    """
    Generates new request ids for use when constructing requests

    The client should send a unique id with each request, so that
    it can later recognize which response belongs to which request,
    in case of having sent multiple requests at a time.
    RequestID is for generating new, unique ids.
    """
    def __iter__(self):
        self.request_id = 1
        return self

    def __next__(self):
        if self.request_id <= 999:
            request_id = self.request_id
            self.request_id += 1
            return request_id
        else:
            # While we may at one time need a client to make
            # thousands of requests at a time, for ICN version the
            # more likely scenario is that someone somewhere made
            # a mistake
            logging.warning("Number of requests are above expected values")
            raise StopIteration


class TCPSocket:
    """
    TCP socket interface.

    Sends and receives json structures to and from a remote server.
    Both input and output are generally assumed to be python objects,
    and this is what the main request(obj) function works with.
    Use:
    Initialize with hostname and port, then use the request(obj) function
    to communicate with the remote server.
    """
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.tcp_socket = None
        self.chunk_length = 32

    def connect(self):
        self.tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        logging.info('Connecting to {} port {}'.format(self.host, self.port))
        self.tcp_socket.connect((self.host, self.port))

    def close(self):
        logging.info('Closing socket')
        self.tcp_socket.close()
        self.tcp_socket = None

    def send(self, message):
        logging.info("Sending request")
        logging.debug(json_prettify(message))
        bytestream = message.encode()
        self.tcp_socket.sendall(bytestream)

    def receive(self):
        logging.info("Receiving response")
        socket_open = True
        received_stream = "".encode()       # Starting with empty bytestream
        while socket_open:
            data_chunk = self.tcp_socket.recv(self.chunk_length)
            received_stream += data_chunk

            # According to standard, empty bytestream is considered EOF
            # TODO: More testing, as it seems no empty bytestream is ever
            # received; detecting on length is a fix.
            # Potential issue:
            # What happens if received message-length has
            # (modulus <self.chunk_length> == 0) ?
            if data_chunk == b'' or len(data_chunk) < self.chunk_length:
                socket_open = False
        received = received_stream.decode()
        logging.debug(json_prettify(received))
        return received

    def msg_request(self, request_msg):
        try:
            self.connect()
            # Since we are client, communication will always happen by us
            # initiating a request, and the server responding.
            # Refactor:
            # Send and receive functions should possibly only be defined
            # inside msg_request since they are dependent upon each other
            # and have no purpose outside of msg_request.
            self.send(request_msg)
            response = self.receive()
        except OSError as ose:
            logging.error("Error #{}: {}".format(ose.errno, ose.strerror))
        finally:
            self.close()
        return response

    def request(self, request_obj):
        request_msg = json.dumps(request_obj)
        response_msg = self.msg_request(request_msg)
        response_obj = json.loads(response_msg)
        return response_obj


class TestCaseBasicFunction(unittest.TestCase):
    # Docstrings avoided since they would be displayed by the
    # unittest testing system as part of the test run.
    #
    # Testing class; performs unit testing to ensure basic
    # functionality.

    def setUp(self):
        host = socket.gethostname()    # Get local machine name
        port = 8080                    # Default port
        self.test_socket = TCPSocket(host, port)

#    def tearDown(self):
#        [code to execute to clean up after tests]

    def test_create_actor(self):
        request = {
            "version": 1,
            "request": 1,
            "function": "new-actor",
            "actor": "/actor-create-test"
        }
        logging.info("Request:\n%s\n", prettify(request))
        response = self.test_socket.request(request)
        logging.info("Response:\n%s\n", prettify(response))
        try:
            assert(response["version"] == 1), "Version mismatch"
            assert(response["request"] == 1), "Request id mismatch"
            assert(response["function"] == "result"), "Not a result"
            assert(response["actor"] == "/actor-create-test"),\
                "Actor not received"
        except AssertionError as ae:
            logging.error(ae)

    def test_get_object(self):
        request = {
            "version": 1,
            "request": 2,
            "function": "get-object",
            "object-id": "/actor-create-test/inbox"
        }
        result_obj = {
            "@id": "/actor-create-test/inbox",
            "@type": "OrderedCollection"
        }
        logging.info("Request:\n%s\n", prettify(request))
        response = self.test_socket.request(request)
        logging.info("Response:\n%s\n", prettify(response))
        try:
            assert(response["version"] == 1), "Version mismatch"
            assert(response["request"] == 2), "Request id mismatch"
            assert(response["function"] == "result"), "Not a result"
            assert(response["object"] == result_obj), "Unexpected result"
        except AssertionError as ae:
            logging.error(ae)

    def test_post_outbox(self):
        request = {
            "version": 1,
            "request": 3,
            "function": "post-outbox",
            "actor": "/actor-create-test",
            "object": {
                "@type": "Create",
                "object": {
                    "@type": "Note",
                    "summary": "test",
                    "content": "blah blah"
                }
            }
        }
        logging.info("Request:\n%s\n", prettify(request))
        response = self.test_socket.request(request)
        logging.info("Response:\n%s\n", prettify(response))
        try:
            assert(response["version"] == 1), "Version mismatch"
            assert(response["request"] == 3), "Request id mismatch"
            assert(response["function"] == "result"), "Not a result"
            assert(is_url(response["object-id"])), "Unexpected result"
        except AssertionError as ae:
            logging.error(ae)


def main():
    # Logging level can be one of DEBUG|INFO|WARNING|ERROR|CRITICAL
    logging_level = logging.WARNING
    logging_format = \
        '%(levelname)-8s - '\
        '%(filename)-10s | '\
        '%(funcName)-8s | '\
        'line %(lineno)3d: '\
        '%(message)s'
    logging.basicConfig(level=logging_level, format=logging_format)

    unittest.main()


# Script entry point; simply runs main() function
# The condition is so that if someone loads this file as a module it is only
# the loading that happens, and we avoid executing the main function.
if __name__ == '__main__':
    main()

# Todo:
# Set up parameter calls for the different functionalities;
# testing, logging and messaging.
